# RUN PROJECT

## sudo docker-compose build

## sudo docker-compose up OR sudo docker-compose up

## KAFKA_UI: localhost:8085

# APIs

# POST

    ## localhost:8080/login
    body: {
        username: "john11",
        password: "hello_world"
    }
    # Response {
    "data": {
        "status": true,
        "message": "You are successfully Logined",
        "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6Ik..."
    }
    ## localhost:8080/registr
    body: {
        name: "John",
        surname: "Smith",
        email: "johnsmith@gmail.com",
        username: "john11",
        password: "hello_world"
    }
    # Response {
    "data": {
        "status":true,
        "message":"You are successfully registered."
    }
}

## POST with AUTH

    ## localhost/auth/math
    ## authoriation: `Bearer token...`
    body {
        "a": 5,
        "b": 6,
        "operation": "*"
    }
