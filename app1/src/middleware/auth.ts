import { Request, Response, NextFunction } from "express";
import { Consumer, KafkaClient, Message, Producer } from "kafka-node";

const JWT_SECRET = process.env.JWT_SECRET || 'mysecret';

interface JwtPayload {
  id: string;
  username: string;
}

class Middleware {
    public static authenticate = (req: Request, res: Response, next: NextFunction) => {
        // Get the token from the Authorization header
        const token = req.headers.authorization?.split(' ')[1];

        console.log("req.headers: ", req.headers);

        if (!token) {
            // If there is no token, return a 401 Unauthorized error
            return res.status(401).json({ error: 'Unauthorized' });
        }

        try {

            const client = new KafkaClient({kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS})
            const producer = new Producer(client)

            const authTopic: string = process.env.KAFKA_TOPIC_Auth || "";
            const authToken: string = req.headers.authorization;


            producer.on('ready', () => {
                producer.send([{
                    topic: authTopic,
                    partition: 0,
                    messages: JSON.stringify(authToken)
                }], async (err, data) => {
                    if(err) {
                        return res.status(401).json({ error: 'Unauthorized' });
                    }
                })

                const consumer = new Consumer(client, [{
                    topic: process.env.KAFKA_TOPIC_Response
                }], { autoCommit: true });

                consumer.on('message', (message: Message) => {
                    console.log('Received message:', JSON.parse(message.value.toString()).status);
                    producer.send([{
                        topic: process.env.KAFKA_TOPIC_Response,
                        partition: message.partition,
                        messages: [null]
                    }], (err, data) => {
                        if (err) {
                            console.error("Errpr: ", err);
                          } else {
                            console.log(`Message removed: ${JSON.stringify(message)}`);
                          }
                    })
                    if (JSON.parse(message.value.toString()).status == "true") {
                        next()
                    } else {
                        return res.status(401).json({ error: 'Unauthorized' });
                    }
                });
                consumer.on('error', (error) => {
                    return res.status(401).json({ error: 'Unauthorized' });
                });
            })
        } catch (error) {
            // If there is an error verifying the token, return a 401 Unauthorized error
            return res.status(401).json({ error: 'Unauthorized' });
        }
    };
}

export default Middleware;
