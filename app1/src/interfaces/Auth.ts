export interface SignIn {
    username: string;
	password: string
}

export interface SignUp {
    name: string;
    surname: string;
    username: string;
	email: string;
	password: string;
}

export interface Middleware {
    authorization: string
}