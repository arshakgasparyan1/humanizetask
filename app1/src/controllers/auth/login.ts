import { Request, Response, Router } from "express"
import { Consumer, KafkaClient, Message, Producer } from 'kafka-node';
import { SignIn } from "../../interfaces/Auth";

class Login {
	public static signIn (req: Request, res: Response): any {
        const client = new KafkaClient({kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS})
        const producer = new Producer(client)

        const loginTopic: string = process.env.KAFKA_TOPIC_Login || "";
        const data: SignIn = req.body;
        producer.on('ready', () => {
            producer.send([{ topic: loginTopic, messages: JSON.stringify(data)}], async (err, data) => {
                if(err) res.send(err)
                else {
                    console.log({data})
                }
            })

            const consumer = new Consumer(client, [{
                topic: process.env.KAFKA_TOPIC_Response
            }], { autoCommit: true });

            consumer.on('message', (message: Message) => {
                producer.send([{
                    topic: loginTopic,
                    partition: message.partition,
                    // @ts-ignore
                    offset: message.offset || 0,
                    messages: [null]
                }], (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                    console.log(`Message removed: ${JSON.stringify(message)}`);
                    }
                })
                res.json({data: JSON.parse(message.value.toString())})
            });

            consumer.on('error', (error) => {
                res.json({error})
            });
        })
	}
}

export default Login;