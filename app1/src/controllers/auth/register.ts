import { Request, Response } from "express"
import { Consumer, KafkaClient, Message, Producer } from 'kafka-node';
import { SignUp } from "../../interfaces/Auth";

class Register {
	public static registration (req: Request, res: Response): any {
		const client = new KafkaClient({kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS})
        const producer = new Producer(client)

        const signUpTopic: string = process.env.KAFKA_TOPIC_Registration || "";
        const data: SignUp = req.body;
        producer.on('ready', () => {
            producer.send([{ topic: signUpTopic, messages: JSON.stringify(data)}], async (err, data) => {
                if(err) res.send(err)
                else {
                    console.log({data})
                }
            })

            const consumer = new Consumer(client, [{ topic: process.env.KAFKA_TOPIC_Response }], { autoCommit: true });

            consumer.on('message', async (message: Message) => {
                producer.send([{
                    topic: signUpTopic,
                    partition: message.partition,
                    // @ts-ignore
                    offset: message.offset || 0,
                    messages: [null]
                }], (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                    console.log(`Message removed: ${JSON.stringify(message)}`);
                    }
                })
                res.json({data: JSON.parse(message.value.toString())})
            });
            consumer.on('error', (error) => {
                res.json({error})
            });
        })
	}
}

export default Register;

