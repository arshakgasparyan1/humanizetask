import * as express from 'express';
import Login from './auth/login';
import Register from './auth/register';
import Middleware from '../middleware/auth';
import Calculator from './auth/calculator';


class Routes {

    public router = express.Router();

    constructor() {
        this.routes();
    };

    private routes = () => {
        this.router.post('/login', Login.signIn);
        this.router.post('/registr', Register.registration);
        this.router.post('/auth/math', Calculator.calc);
    };
}

export default new Routes().router;