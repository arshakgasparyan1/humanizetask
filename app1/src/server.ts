import * as http from 'http';
import * as dotenv from "dotenv";
dotenv.config();

import app from './app';
import KafkaAdminService from './KafkaClient/kafkaClient';

let server: http.Server<typeof http.IncomingMessage, typeof http.ServerResponse>;
const runClusters = () => {
  KafkaAdminService.createTopic()
  // listen on port
  server = http.createServer(app).listen(process.env.PORT, () => {
    console.log(`Server ${process.pid} started on port ${process.env.PORT}`);
  });
};

runClusters();

export default server;