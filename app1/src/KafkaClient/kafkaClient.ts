import * as kafka from 'kafka-node';


class KafkaAdminService {
	public static createTopic (): any {
        const client = new kafka.KafkaClient({ kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS });

        const topics: string[] = [
            process.env.KAFKA_TOPIC_Login,
            process.env.KAFKA_TOPIC_Registration,
            process.env.KAFKA_TOPIC_Auth,
            process.env.KAFKA_TOPIC_MathFunctionality,
            process.env.KAFKA_TOPIC_Response
        ]

        const producer = new kafka.Producer(client);

        producer.on('ready', () => {
            producer.createTopics(topics, (err, data) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log(`Topics ${topics} created`);
            });
        });
	}
}

export default KafkaAdminService;