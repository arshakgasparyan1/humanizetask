import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
import * as methodOverride from 'method-override';
import * as path from 'path';
import routes from './controllers';


class Server {
    public app = express();

    constructor() {
        this.config();
        this.routes();
    };
    private config() {
        /** Enabling cross-origin resource sharing */
        this.app.use(cors({ origin: true }));
        /** Logging api calls */
        this.app.use(morgan('dev'));

        /** Enabling middleware that parses json */
        this.app.use(express.json({ limit: '5mb' }), (err, req: express.Request, res: express.Response, next: express.NextFunction) => {
          if (err && err.status === 400) return res.status(400).send(err.type);
        });
        /** Enabling method-override */
        this.app.use(methodOverride());
        /** Opening media folder */
        this.app.use('/', express.static(path.join(__dirname, 'dist')));
    }

    private routes() {
        this.app.use('/', routes);
    }
}

export default new Server().app;