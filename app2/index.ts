import "reflect-metadata"
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import kafka, { OffsetFetchRequest } from 'kafka-node';
import connection from "./src/DBConnect";
import Login from "./src/controller/login";
import Registration from "./src/controller/register";
import { IResponse } from "./src/interfaces/Response";
import { authenticateMiddleware } from "./src/controller/checkAuth";

const app: Express = express();
const port = process.env.PORT;



app.get('/', (req: Request, res: Response) => {
  res.send('Express + TypeScript Server');
});
const isRunning = async () => {
  dotenv.config();
  connection.initialize().then(() => {
      console.log("DB is working");
  })
  .catch((error) => console.log("Error: ", error))

  const client = new kafka.KafkaClient({kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS})
  const topics: OffsetFetchRequest[] = [
      {topic: process.env.KAFKA_TOPIC_Login || "", partition: 0},
      {topic: process.env.KAFKA_TOPIC_Registration || "", partition: 0},
      {topic: process.env.KAFKA_TOPIC_MathFunctionality || "", partition: 0},
      {topic: process.env.KAFKA_TOPIC_Response || "", partition: 0},
      {topic: process.env.KAFKA_TOPIC_Auth || "", partition: 0}
  ]

  const responseTopic = process.env.KAFKA_TOPIC_Response || "";
  const consumer = new kafka.Consumer(client, topics, {autoCommit: false})
  let result: IResponse;

  consumer.on('message', async (message) => {
    const value = message.value ? JSON.parse(message.value.toString()) : null;

    const producer = new kafka.Producer(client);

    if (value && message.topic == process.env.KAFKA_TOPIC_Login) {
      result = await Login(value);
      producer.send([{
        topic: process.env.KAFKA_TOPIC_Login,
        partition: message.partition,
        //@ts-ignore
        offset: Number(message.offset) || 1,
        messages: [null],
      }], (err, data) => { console.log(err ? err : data) })
    }
    if (value && message.topic == process.env.KAFKA_TOPIC_Registration) {
      result = await Registration(value)
      producer.send([{
        topic: process.env.KAFKA_TOPIC_Registration,
        partition: message.partition,
        //@ts-ignore
        offset: Number(message.offset) || 1,
        messages: [null],
      }], (err, data) => { console.log(err ? err : data) })
    }

    if (value && message.topic == process.env.KAFKA_TOPIC_Auth) {
      result = await authenticateMiddleware(value)
      producer.send([{
        topic: process.env.KAFKA_TOPIC_Auth,
        partition: message.partition,
        //@ts-ignore
        offset: Number(message.offset) || 1,
        messages: [null],
      }], (err, data) => { console.log(err ? err : data) })
    }


    producer.send([{
      topic: responseTopic,
      partition: 0,
      messages: JSON.stringify(result)
    }], async (err, data) => {
      if (err) {
        console.error('Error sending data:', err);
      } else {
        console.log('Data sent:', data);
      }
    })
})

  consumer.on('error', (err) => {
      console.log(err)
  })
}
setTimeout(isRunning, 20000)


app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

