export interface User {
    id: number;
    username: string;
    password: string;
}

export interface UserLogin {
    username: string;
    password: string;
}

export interface UserRegistration {
    name: string;
    surname: string;
    email: string;
    username: string;
    password: string;
}