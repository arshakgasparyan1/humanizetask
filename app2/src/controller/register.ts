import bcrypt from "bcrypt";
import { UserRegistration } from "../interfaces/User";
import connection from "../DBConnect";
import { Users } from "../entities/User";
import { IResponse } from "../interfaces/Response";


export const findUserByUsername = async (username: string, email: string): Promise<Users | null> => {
    const query = connection.getRepository(Users).createQueryBuilder("users")
    .where("users.username = :username", { username: username })
    .orWhere("users.email = :email", { email: email })
    .getOne();
    return await query;
};



const Registration = async (data: UserRegistration): Promise<IResponse> => {
    const user = await findUserByUsername(data.username, data.email);
    const result: IResponse = {
        status: false,
        message: "try again"
    }
    if (!user) {
        const hashedPassword = await bcrypt.hash(data.password, 10);
        let newUser: Users = new Users();
        newUser.name = data.name,
        newUser.surname = data.surname,
        newUser.email = data.email,
        newUser.username = data.username,
        newUser.password = hashedPassword,
        await connection.manager.save(newUser)
        result.status = true;
        result.message = "You are successfully registered."
    } else {
        result.message = "such username or EMail arleady registered."
    }
    return result;
};

export default Registration