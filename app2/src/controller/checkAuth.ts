import jwt from "jsonwebtoken";
import { IResponse } from "../interfaces/Response";


const JWT_SECRET = process.env.JWT_SECRET || 'mysecret';


interface JwtPayload {
  id: number;
  username: string;
  email: string;
}

export const authenticateMiddleware = async (jwtToken: string): Promise<IResponse> => {
    const token = jwtToken.split(' ')[1];
    let result: IResponse = {
      status: true,
      message: "ok"
    };
    try {
        // Verify the token and extract the payload
        console.log("token: ", token);
        
        const payload = jwt.verify(token, JWT_SECRET) as JwtPayload;
    } catch (error) {
      result.status = false;
      result.message = "Unauthorized";
    }
    console.log(result);
    
    return result;
};