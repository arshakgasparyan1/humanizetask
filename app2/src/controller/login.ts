import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import connection from "../DBConnect";
import { Users } from "../entities/User";
import { UserLogin } from "../interfaces/User";
import { IResponse } from "../interfaces/Response";


const JWT_SECRET = process.env.JWT_SECRET || 'mysecret';

export const findUserByUsername = async (username: string): Promise<Users | null> => {
    const query = connection.getRepository(Users).createQueryBuilder("users")
    .where("users.username = :username", { username: username })
    .orWhere("users.email = :username", { username: username })
    .getOne();
    return await query;
};

export const createToken = (payload: any) => {
    return jwt.sign(payload, JWT_SECRET, { expiresIn: 86400 });
};

const Login = async (userData: UserLogin): Promise<IResponse> => {
    const user = await findUserByUsername(userData.username)
    const result: IResponse = {
        status: false,
        message: "Wrong Email or Surname"
    }
    if (user) {
        const match: boolean = await bcrypt.compare(userData.password, user.password);
        if (match) {
            const token = createToken({
                id: user.id,
                email: user.email,
                username: user.username
            })
            result.status = true
            result.message = "You are successfully Logined"
            result.data = token
        } else {
            result.message = "Wrong Password"
        }
    }
    return result;
}

export default Login