import { Users } from "../entities/User";
import { DataSource } from "typeorm";

interface Config {
  url: string;
  entities: any;
  synchronize: boolean;
  logging: boolean;
}


const dbConfig: Config = {
  url:  process.env.POSTGRES_URL || "postgres://postgres:postgres@localhost:5432/myTest",
  entities: [Users],
  synchronize: true,
  logging: false
}

const connection = new DataSource({
    type: "postgres",
    ...dbConfig
})

export default connection