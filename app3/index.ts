import "reflect-metadata"
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import kafka, { OffsetFetchRequest } from 'kafka-node';
import connection from "./src/DBConnect";
import { IResponse } from "./src/interfaces/Response";
import Calculator from "./src/controller/mathLogic"
const app: Express = express();
const port = process.env.PORT;



app.get('/', (req: Request, res: Response) => {
  res.send('Express + TypeScript Server');
});
const isRunning = async () => {
  dotenv.config();
    connection.initialize().then(() => {
        console.log("DB is working");
    })
    .catch((error) => console.log("Error: ", error))

    const client = new kafka.KafkaClient({kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS})
    const topics: OffsetFetchRequest[] = [
        {topic: process.env.KAFKA_TOPIC_MathFunctionality || ""}
    ]
    // const topic1 = process.env.KAFKA_TOPIC1 || "";
    const responseTopic = process.env.KAFKA_TOPIC_Response || "";
    const consumer = new kafka.Consumer(client, topics, {autoCommit: false})

    consumer.on('message', async (message) => {
        const value = message.value ? JSON.parse(message.value.toString()) : null;

        let result: IResponse = {
          message: "Can'not calculate.",
          status: false
        };
        const producer = new kafka.Producer(client);
        if (message.value && message.topic == process.env.KAFKA_TOPIC_MathFunctionality) {
          switch (value.method) {
            case "+":
              result.data = Calculator.add(value.a, value.b)
              result.status = true
              break;
            case "-":
              result.data = Calculator.subtract(value.a, value.b)
              result.status = true
              break;
            case "/":
              if (value.b === 0) {
                result.message = "Cannot divide by zero."
                result.status = false
              } else {
                result.data = Calculator.divide(value.a, value.b)
                result.status = true
              }
              break;
            case "*":
              result.data = Calculator.multiply(value.a, value.b)
              result.status = true
              break;
            default:
              break;
          }
          producer.send([{
            topic: process.env.KAFKA_TOPIC_MathFunctionality,
            partition: message.partition,
            //@ts-ignore
            offset: Number(message.offset) || 1,
            messages: [null],
          }], (err, data) => { console.log(err ? err : data) })
        }
        producer.send([{
          topic: responseTopic,
          partition: message.partition,
          //@ts-ignore
          offset: message.offset,
          messages: JSON.stringify(result)
        }], (err, data) => {
              if (err) {
                  console.error('Error sending data:', err);
              } else {
                  console.log('Data sent:');
              }
          });
    })

  consumer.on('error', (err) => {
      console.log(err)
  })
}
setTimeout(isRunning, 20000)


app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

