import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";

@Entity()
export class Math {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    input1!: number;

    @Column()
    input2!: number;

    @Column()
    method!: string;

    @Column()
    result!: number;


    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date | undefined;
}